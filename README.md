# demo-api

OpenAPI definition of the demo API. Ansible is implemented to generate spring application with api definication.

## Get started

- Edit/View/Test the api defined in the project

    - [Swagger editor](https://editor.swagger.io/) is used to open the project. It will render your API specification visually and interact with your API immediately. After done with the API contracts, you can use codegen tool to generate server stubs or client libraries for your API in most popular languages as well.

    - [Swagger-node](https://github.com/swagger-api/swagger-node) is an another tool to help us edit/view/test your API.(In process)

## Usage of demo  api

### **/demoapi/v1/jobs**

Request a job to start.

#### Request

```
method: POST,
headers: {
  accept-type: application/json
  content-type: application/json
},
body: {
    "jobName": 'jobName,
    "jobParam": {}
}
```

#### Result

```
headers: {
  content-type: application/json
},
body: {
  "jobID": "JOBID"
}
```
### **/demoapi/v1/jobs**

Gets the list of all jobs

#### Request

```
method: GET,
headers: {
  accept-type: application/json
  content-type: application/json
}
```

#### Result

```
headers: {
  content-type: application/json
},
body: {jobs:{}}
```

### **/demoapi/v1/jobs/{jobID}/status**

Gets the status of a job

#### Request

```
method: GET,
headers: {
  accept-type: application/json
  content-type: application/json
}
```

#### Result

```
headers: {
  content-type: application/json
},
body: {
  "status": "RUNNING | COMPLETED | ERROR"
}
```

### **/demoapi/v1/jobs/{jobID}/result**

Gets the result of a job

#### Request

```
method: GET,
headers: {
  accept-type: application/json
  content-type: application/json
}
```

#### Result

```
headers: {
  content-type: application/json
},
body: {
  "result": "RESULT_HERE"
}
```

### **/demoapi/v1/jobs/{jobID}/stop**

Stops a job

#### Request

```
method: POST,
headers: {
  accept-type: application/json
  content-type: application/json
},
body: {}
```

#### Result

```
headers: {
  content-type: application/json
},
body: {}
```
### **/demoapi/v1/jobs/{jobID}/restart**

Restarts a job using the previous params

#### Request

```
method: POST,
headers: {
  accept-type: application/json
  content-type: application/json
},
body: {}
```

#### Result

```
headers: {
  content-type: application/json
},
body: {}
```

### **/demoapi/v1/jobs/{jobID}/dispose**

Disposes a job resources

#### Request

```
method: DELETE,
headers: {
  accept-type: application/json
  content-type: application/json
},
body: {}
```

#### Result

```
headers: {
  content-type: application/json
},
body: {}
```
